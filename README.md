# CAD for VLSI Tool Setup

Requirements: Minimum Ubuntu 20, Internet connection

## Python dependency setup:
```sh
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev git curl

$  curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
```

Add the following at the end of ~/.bashrc
```sh
 export PATH="/home/$USER/.pyenv/bin:$PATH"
 eval "$(pyenv init -)"
 eval "$(pyenv init --path)"
 eval "$(pyenv virtualenv-init -)"
 ```

for python 3.6 
```sh 
$ CONFIGURE_OPTS=--enable-shared pyenv install 3.8.10
$ pyenv virtualenv 3.8.10 py38
```

Activate env
```sh
$ pyenv activate py38
```

Please use the following setup for all CoCoTb based Verification.
- CoCoTb version in pyenv
```sh
$ pip install cocotb==1.6.2
```

## Bluespec Installation

```sh
# BSC Installation:

# Tested in Ubuntu 20
$ apt-get install ghc \
    libghc-regex-compat-dev \
    libghc-syb-dev \
    libghc-old-time-dev \
    libghc-split-dev \
    tcl-dev \
    build-essential \
    pkg-config \
    autoconf \
    gperf \
    flex \
    bison 

$ cd $HOME
$ git clone --recursive https://github.com/B-Lang-org/bsc
$ cd bsc
$ git checkout 2021.12.1
$ sudo make install-src

# Test whether properly installed
$ bsc -help 

```
## Verilator Installation

```sh
# Dependencies
$ sudo apt-get install git perl python3 make autoconf g++ flex bison ccache
$ sudo apt-get install libgoogle-perftools-dev numactl perl-doc
$ sudo apt-get install libfl2  # Ubuntu only (ignore if gives error)
$ sudo apt-get install libfl-dev  # Ubuntu only (ignore if gives error)
$ sudo apt-get install zlibc zlib1g zlib1g-dev  # Ubuntu only (ignore if gives error)

# Clone the stable version and install

$ git clone https://github.com/verilator/verilator 
$ cd verilator
$ git checkout v4.106      # Use most recent stable release for CoCoTb
$ unset VERILATOR_ROOT

$ autoconf        # Create ./configure script
$ ./configure     # Configure and create Makefile
$ make -j `nproc`        # Build Verilator itself
$ sudo make install
```
