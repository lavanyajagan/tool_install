## Python dependency setup:
```sh
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev git curl

$  curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
```

Add the following at the end of ~/.bashrc
```sh
 export PATH="/home/$USER/.pyenv/bin:$PATH"
 eval "$(pyenv init -)"
 eval "$(pyenv init --path)"
 eval "$(pyenv virtualenv-init -)"
 ```

for python 3.6 
```sh 
$ CONFIGURE_OPTS=--enable-shared pyenv install 3.8.10
$ pyenv virtualenv 3.8.10 py38
```

Activate env
```sh
$ pyenv activate py38
```

# C-Class

```
$ git clone https://gitlab.com/shaktiproject/cores/c-class
$ pyenv activate py38
$ cd c-class
$ pip install -r requirements.txt
$ python -m configure.main  -ispec sample_config/c64/rv64i_isa.yaml   -customspec sample_config/c64/rv64i_custom.yaml   -cspec sample_config/c64/core64.yaml   -gspec sample_config/c64/csr_grouping64.yaml   -dspec sample_config/c64/rv64i_debug.yaml   --verbose debug
$ make generate_verilog; make link_verilator generate_boot_files 

# running a single test
$ make test opts='--test=add --suite=rv64ui --debug' CONFIG_ISA=RV64IMAFDC
```

# running aapg
```
# generate
$ make aapg opts='--config=rv64imafdc_bringup_u --test_count=1' CONFIG_ISA=RV64IMAFDC

# simulate
$ make regress opts='--list=aapg.list --sub --test_opts="--timeout=15m" --debug' CONFIG_ISA=RV64IMAFDC HYP=1 TO=500
```